import Vue from 'vue'
import Default from '@/layouts/Default.vue'
import PageDefault from '@/layouts/page/Default.vue'
import PageBlank from '@/layouts/page/Blank.vue'
import PageHome from '@/layouts/page/Home.vue'
import Post from '@/layouts/post/Default.vue'
import UserDefault from '@/layouts/user/Default.vue'
import AdminDefault from '@/layouts/admin/Default.vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/sass/tailwind.sass'

// Layouts for dynamic component
Vue.component('default-layout', Default)
Vue.component('page-default-layout', PageDefault)
Vue.component('page-blank-layout', PageBlank)
Vue.component('page-home-layout', PageHome)
Vue.component('post-layout', Post)
Vue.component('user-default-layout', UserDefault)
Vue.component('admin-default-layout', AdminDefault)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
