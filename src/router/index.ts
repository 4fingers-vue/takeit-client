import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const isLoggedIn = () => store.getters['userInfo/isUserLoggedIn']
const isAdmin = () => store.getters['userInfo/getLevel'] === 1

const routes: Array<RouteConfig> = [
  // Base pages
  {
    path: '*',
    meta: { layout: 'page-blank' },
    component: () => import('@/views/pages/error/404.vue'),
  },
  {
    path: '/404',
    name: '404',
    meta: { layout: 'page-blank' },
    component: () => import('@/views/pages/error/404.vue'),
  },
  {
    path: '/',
    name: 'Home',
    meta: { layout: 'page-home' },
    component: () => import('@/views/pages/Home.vue'),
  },
  {
    path: '/profile',
    name: 'Profile',
    meta: { layout: 'user-default' },
    component: () => import(/* webpackChunkName: "about" */ '@/views/user/Profile.vue'),
  },
  {
    path: '/about',
    name: 'About',
    meta: { layout: 'page-default' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/pages/About.vue'),
  },
  // Posts
  {
    path: '/posts',
    meta: { layout: 'post' },
    name: 'posts',
    component: () => import('@/views/post/Browse.vue'),
  },
  {
    path: '/post/:id',
    meta: { layout: 'post' },
    name: 'post-detail',
    component: () => import('@/views/post/Detail.vue'),
    props: true,
  },
  // User routes
  {
    path: '/user',
    component: () => import('@/views/user/Login.vue'),
  },
  {
    path: '/user/login',
    component: () => import('@/views/user/Login.vue'),
    name: 'user-login',
  },
  // Dashboard
  {
    path: '/user/dashboard',
    name: 'user-dashboard',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/Dashboard.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  // Item
  {
    path: '/user/items',
    name: 'user-items',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/item/Manage.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  {
    path: '/user/item/add',
    name: 'user-items-add',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/item/Add.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  {
    path: '/user/item/edit/:id',
    name: 'user-items-edit',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/item/Edit.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
    props: true,
  },
  // Post
  {
    path: '/user/posts',
    name: 'user-posts',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/post/Manage.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  {
    path: '/user/post/add',
    name: 'user-posts-add',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/post/Add.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  // Transaction
  {
    path: '/user/transactions',
    name: 'user-transactions',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/transaction/Manage.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  // Response
  {
    path: '/user/responses',
    name: 'user-responses',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/response/Manage.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  // Settings
  {
    path: '/user/settings',
    name: 'user-settings',
    meta: { layout: 'user-default' },
    component: () => import('@/views/user/Settings.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn()) next()
      else next('/404')
    },
  },
  // Admin routes
  {
    path: '/admin/dashboard',
    name: 'admin-dashboard',
    meta: { layout: 'page-blank' },
    component: () => import('@/views/admin/Dashboard.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
  {
    path: '/admin/analystics',
    name: 'admin-analystics',
    meta: { layout: 'admin-default' },
    component: () => import('@/views/admin/Analysis.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
  {
    path: '/admin/items',
    name: 'admin-items',
    meta: { layout: 'admin-default' },
    component: () => import('@/views/admin/Items.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
  {
    path: '/admin/posts',
    name: 'admin-posts',
    meta: { layout: 'admin-default' },
    component: () => import('@/views/admin/Posts.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
  {
    path: '/admin/transactions',
    name: 'admin-transactions',
    meta: { layout: 'admin-default' },
    component: () => import('@/views/admin/Transactions.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
  {
    path: '/admin/users',
    name: 'admin-users',
    meta: { layout: 'admin-default' },
    component: () => import('@/views/admin/Users.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
  {
    path: '/admin/responses',
    name: 'admin-responses',
    meta: { layout: 'admin-default' },
    component: () => import('@/views/admin/Responses.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
  {
    path: '/admin/settings',
    name: 'admin-settings',
    meta: { layout: 'admin-default' },
    component: () => import('@/views/admin/Settings.vue'),
    beforeEnter: (to, from, next) => {
      if (isLoggedIn() && isAdmin()) next()
      else next('/404')
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  routes,
})

export default router
