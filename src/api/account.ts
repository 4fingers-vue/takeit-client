import axios from 'axios'

const baseURL = 'http://localhost:4000/api/users'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class AccountsAPI {
  static authenticate = (usernameIn: string, passwordIn: string) => axios.post(`${baseURL}/authenticate`, {
    username: usernameIn,
    password: passwordIn,
  })

  static register = (usernameIn: string, passwordIn: string) => axios.post(baseURL, {
    username: usernameIn,
    password: passwordIn,
  })

  static filter = (stateIn: number, returnTypeIn: number, token: string) => {
    let url = `${baseURL}/filter?`

    if (stateIn !== -1) {
      url += `&state=${stateIn}`
    }

    if (returnTypeIn !== -1) {
      url += `&returnType=${returnTypeIn}`
    }

    return axios.get(url, configWithBearerToken(token))
  }

  static update = (idIn: string, firstNameIn: string, lastNameIn: string, usernameIn: string,
    hashIn: string, tokenIn: string, levelIn: number, stateIn: number) => axios.put(`${baseURL}/${idIn}`, {
    id: idIn,
    firstName: firstNameIn,
    lastName: lastNameIn,
    username: usernameIn,
    hash: hashIn,
    token: tokenIn,
    level: levelIn,
    state: stateIn,
  },
  configWithBearerToken(tokenIn))

  static getInfo = (idIn: string, token: string) => axios.get(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
