import axios from 'axios'

const baseURL = 'http://localhost:4000/api/provinces'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class ItemsAPI {
  static filter = (nameIn: string) => {
    if (nameIn) {
      return axios.get(`${baseURL}/filter?name=${nameIn}`)
    }
    return axios.get(`${baseURL}/filter`)
  }

  static getItem = (nameIn: string) => axios.get(`${baseURL}/${nameIn}`)

  static add = (nameIn: string, token: string) => axios.post(`${baseURL}/`, {
    name: nameIn,
  },
  configWithBearerToken(token))

  static edit = (idIn: string, nameIn: string, token: string) => axios.put(`${baseURL}/${idIn}`, {
    name: nameIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: string, token: string) => axios.delete(`${baseURL}/${idIn}`,
    configWithBearerToken(token))
}
