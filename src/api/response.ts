import axios from 'axios'

const baseURL = 'http://localhost:4000/api/responses'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class ResponsessAPI {
  static filter = (typeIn: number, returnTypeIn: number, token: string) => {
    let url = `${baseURL}/filter?`

    if (typeIn !== -1) {
      url += `type=${typeIn}`
    }

    url += `&returnType=${returnTypeIn}`

    return axios.get(url, configWithBearerToken(token))
  }

  static getResponse = (responseIdIn: string, returnTypeIn: number) => axios.get(`${baseURL}/${responseIdIn}?returnType=${returnTypeIn}`)

  static create = (reporterIn: string, subjectIn: string, typeIn: number, token: string) => axios
    .post(
      baseURL, {
        reporter: reporterIn,
        subject: subjectIn,
        content: '',
        type: typeIn,
      },
      configWithBearerToken(token),
    )

  static update = (idIn: string, reporterIn: string, subjectIn: string, contentIn: string, adminMessageIn: string, createdAtIn: string, typeIn: number, stateIn: number, token: string) => axios.put(`${baseURL}/${idIn}`, {
    id: idIn,
    reporter: reporterIn,
    subject: subjectIn,
    content: contentIn,
    adminMessage: adminMessageIn,
    createdAt: createdAtIn,
    type: typeIn,
    state: stateIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: string, token: string) => axios.delete(`${baseURL}/${idIn}`,
    configWithBearerToken(token))
}
