import axios from 'axios'

const baseURL = 'http://localhost:4000/api/items'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class ItemsAPI {
  static filter = (ownerIn: string, stateIn: number, returnTypeIn: number, token: string) => {
    let url = `${baseURL}/filter?`

    if (ownerIn) {
      url += `owner=${ownerIn}`
    }

    if (stateIn !== -1) {
      url += `&state=${stateIn}`
    }

    if (returnTypeIn !== -1) {
      url += `&returnType=${returnTypeIn}`
    }

    return axios.get(url, configWithBearerToken(token))
  }

  static getItems = () => axios.get(baseURL)

  static getItem = (itemIdIn: string, returnTypeIn: number, token: string) => axios.get(`${baseURL}/${itemIdIn}?returnType=${returnTypeIn}`,
    configWithBearerToken(token))

  static add = (ownerIn: string, nameIn: string, quantityIn: number, unitIn: string, imagesIn: Array<string>, token: string) => axios.post(`${baseURL}/`, {
    owner: ownerIn,
    name: nameIn,
    quantity: quantityIn,
    unit: unitIn,
    images: imagesIn,
  },
  configWithBearerToken(token))

  static update = (idIn: string, ownerIn: string, nameIn: string, quantityIn: number, unitIn: string, imagesIn: Array<string>, stateIn: number, token: string) => axios.put(`${baseURL}/${idIn}`, {
    id: idIn,
    owner: ownerIn,
    name: nameIn,
    quantity: quantityIn,
    unit: unitIn,
    images: imagesIn,
    state: stateIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: string, token: string) => axios.delete(`${baseURL}/${idIn}`,
    configWithBearerToken(token))
}
