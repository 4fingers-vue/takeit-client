import axios, { AxiosRequestConfig } from 'axios'

const baseURL = 'http://localhost:4000/api/posts'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class PostsAPI {
  // Public
  static getPosts = (typeIn: number) => {
    let url = `${baseURL}/filter?`

    if (typeIn !== null) {
      url += `type=${typeIn}&state=1&returnType=2`
    } else {
      url += 'state=1&returnType=2'
    }

    return axios.get(url)
  }

  // User
  static filter = (ownerIn: string, typeIn: number, categoryIn: string, provinceIn: string,
    stateIn: number, returnTypeIn: number, token: string) => {
    let url = `${baseURL}/filter?`

    if (ownerIn) {
      url += `owner=${ownerIn}`
    }

    if (typeIn !== -1) {
      url += `&type=${typeIn}`
    }

    if (categoryIn) {
      url += `&category=${categoryIn}`
    }

    if (provinceIn) {
      url += `&province=${provinceIn}`
    }

    if (stateIn !== -1) {
      url += `&state=${stateIn}`
    }

    if (returnTypeIn !== -1) {
      url += `&returntype=${returnTypeIn}`
    }

    return axios.get(url, configWithBearerToken(token))
  }

  static getPost = (postIdIn: string, returnTypeIn: number) => axios.get(`${baseURL}/${postIdIn}?returnType=${returnTypeIn}`)

  static add = (ownerIn: string, categoryIn: string, provinceIn: string, typeIn: number, titleIn: string, contentIn: string, locationIn: string, itemsIn: Array<any>, token: string) => axios.post(`${baseURL}/`, {
    owner: ownerIn,
    category: categoryIn,
    province: provinceIn,
    type: typeIn,
    title: titleIn,
    content: contentIn,
    location: locationIn,
    items: itemsIn,
  },
  configWithBearerToken(token))

  static update = (idIn: string, ownerIn: string, categoryIn: string, provinceIn: string, typeIn: number, titleIn: string, contentIn: string, locationIn: string, itemsIn: Array<any>, stateIn: number, token: string) => axios.put(`${baseURL}/${idIn}`, {
    id: idIn,
    owner: ownerIn,
    category: categoryIn,
    province: provinceIn,
    type: typeIn,
    title: titleIn,
    content: contentIn,
    location: locationIn,
    items: itemsIn,
    state: stateIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: string, token: string) => axios.delete(`${baseURL}/${idIn}`,
    configWithBearerToken(token))
}
