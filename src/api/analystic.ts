import axios from 'axios'

const baseURL = 'http://localhost:4000/api/analystics'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class AnalysticsAPI {
  static getOverAllData = (token: string) => axios.get(`${baseURL}/overall`,
    configWithBearerToken(token))

  static getChartData = (typeIn: number, token: string) => axios.get(`${baseURL}/chart?type=${typeIn}`,
    configWithBearerToken(token))
}
