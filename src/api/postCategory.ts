import axios from 'axios'

const baseURL = 'http://localhost:4000/api/postcategories';

export default class PostCategoriesAPI {
  // Public
  static filter = () => axios.get(`${baseURL}/filter`)
}
