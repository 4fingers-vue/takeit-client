import axios from 'axios'

const baseURL = 'http://localhost:4000/api/transactions'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class TransactionsAPI {
  static filter = (userIdIn: string, stateIn: number, returnTypeIn: number, token: string) => {
    let url = `${baseURL}/filter?`

    if (userIdIn) {
      url += `userId=${userIdIn}`
    }

    if (stateIn !== -1) {
      url += `&state=${stateIn}`
    }

    url += `&returnType=${returnTypeIn}`

    return axios.get(url, configWithBearerToken(token))
  }

  static getTransaction = (transactionIdIn: string, returnTypeIn: number) => axios.get(`${baseURL}/${transactionIdIn}?returnType=${returnTypeIn}`)

  static create = (giverIn: string, receiverIn: string, postIn: string, token: string) => axios
    .post(
      baseURL, {
        giver: giverIn,
        receiver: receiverIn,
        post: postIn,
      },
      configWithBearerToken(token),
    )

  static update = (idIn: string, giverIn: string, receiverIn: string, postIn: string,
    createdAtIn: string, stateIn: number, token: string) => axios.put(`${baseURL}/${idIn}`, {
    giver: giverIn,
    receiver: receiverIn,
    post: postIn,
    createdAt: createdAtIn,
    state: stateIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: string, token: string) => axios.delete(`${baseURL}/${idIn}`,
    configWithBearerToken(token))
}
