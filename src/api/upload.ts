import axios from 'axios'

const baseURL = 'http://localhost:4000/api';

export default class UploadAPI {
  static upload = (form: any, token: string) => axios.post(`${baseURL}/upload`, form, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}
