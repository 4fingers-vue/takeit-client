export default {
  namespaced: true,
  state: () => ({
    theme: localStorage.getItem('theme') || 'light',
  }),
  mutations: {
    changeTheme(state: any, value: string) {
      state.theme = value
    },
  },
  actions: {
    changeTheme(context: any, value: string) {
      context.commit('changeTheme', value)
      localStorage.setItem('theme', value)
    },
  },
  getters: {
    isDarkTheme(state: any) {
      return state.theme === 'dark'
    },
  },
}
