export default {
  namespaced: true,
  state: () => ({
    userName: localStorage.getItem('userName') || '',
    userId: localStorage.getItem('userId') || '',
    level: parseInt(localStorage.getItem('userLevel')!, 10) || 0,
    token: localStorage.getItem('userToken') || '',
  }),
  mutations: {
    changeUserName(state: any, userName: string) {
      state.userName = userName
    },
    changeUserId(state: any, userId: string) {
      state.userId = userId
    },
    changeLevel(state: any, level: number) {
      state.level = level
    },
    changeToken(state: any, token: string) {
      state.token = token
    },
  },
  actions: {
  },
  getters: {
    getLevel(state: any): number {
      return state.level
    },
    isUserLoggedIn(state: any): boolean {
      return state.token !== ''
    },
  },
}
