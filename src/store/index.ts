import Vue from 'vue'
import Vuex from 'vuex'
import viewModule from './modules/public/view'
import infoModule from './modules/user/info'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    isRegisterNow: false,
    postType: 0,
    postStyle: 1,
  },
  mutations: {
    changeRegisterNow(state, isRegisterNow) {
      state.isRegisterNow = isRegisterNow
    },
    changePostType(state, type) {
      state.postType = type
    },
    changePostStyle(state, style) {
      state.postStyle = style
    },
  },
  actions: {
  },
  modules: {
    userInfo: infoModule,
    view: viewModule,
  },
})

export default store
