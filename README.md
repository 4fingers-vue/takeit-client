# takeit-client

Vue frontend for TAKEIT project - my graduation thesis. Created with `vue-cli` and do anything from scratch instead of `Nuxt.js`.

**Warning**: This is not finished yet. I'll update later.

## Technical using
### Default:
- PostCSS
- AirBnb ES lint rules
- Typescript
- Babel

### Customizes:
- Tailwind CSS
- SASS
- Axios
- MinIO (for uploading to MinIO server)
- Chart.js

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Related projects
[TAKEIT](https://gitlab.com/4fingers-dotnet-core/take-it)
